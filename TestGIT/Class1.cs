﻿namespace TestGIT
{
    public class Helper
    {
        public static int Add(int a, int b)
        {
            return a + b;
        }

        public static int Divide(int a, int b)
        {
            return a / b;
        }

        public static int Subtract(int a, int b)
        {
            return a - b;
        }
    }
}
